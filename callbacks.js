function logValue(value){
    console.log(value);
}

function logValueWithMessage(value){
    console.log('Message: ' +  value);
}

logValue('Hello World!');
logValueWithMessage('Hello World!');

function log(loggingFunction, value) {
    loggingFunction(value);
}

log(logValue, 'Hello');
log(logValueWithMessage, 'Hello');
log(function () {
    console.log('This is a custom message');
}, 'Hello');