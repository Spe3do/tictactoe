'use strict';
let custom = {};
custom.toString = () => 'Hello Béla';
let custom2 = {};
// console.log('custom: ' + custom);
// console.log('custom: ' + custom2);
/*
function Animal(name) {
    console.log(this, arguments[0], arguments[2]);
    this.name = name;
    console.log(this);
    this.getAnimalName = function () {
        return this.name;
    }
}
Animal.prototype.getName = function () {
    return this.name;
}

function Dog(){
    this.bark = function () {
        console.log('Vau');
    }
}

Dog.prototype = Animal;
*/
class Animal{
    constructor(name){
        this.name = name;
    }
    getName(){
        return this.name;
    }
}

class Dog extends Animal {
    bark(){
        return 'Vau';
    }
}


// Animal('cica', 'kutya');
// Animal.call({type: 'animal'}, 'cica', 'kutya');
// Animal.apply({type: 'dog'}, ['cica', null ,'kutya']);

let cat = new Animal('Mirci');
console.log('cat', cat.getName());
let dog = new Dog('Buksi');