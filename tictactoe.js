class Board {
    constructor() {
        this.fields = [
            [null, null, null],
            [null, null, null],
            [null, null, null]
        ];
    }

    place(x, y, mark) {
        checkBound(x, 'X', 0, 2);
        checkBound(y, 'Y', 0, 2);
        if (!this.fields[x][y]) {
            console.log(mark + " added to x: " + x + " y: " + y);
            this.fields[x][y] = mark;
        }
    }

    fieldEmpty(x, y) {
        return !this.fields[x][y] ? true : false;
    }


    getAllWinningPossibilites() {
        let rows = this.fields;
        let columns = [];
        for (let i = 0; i < this.fields.length; i++) {
            columns.push([
                this.fields[0][i],
                this.fields[1][i],
                this.fields[2][i]
            ]);
        }
        return rows.concat(columns, [
            [this.fields[0][0], this.fields[1][1], this.fields[2][2]],
            [this.fields[0][2], this.fields[1][1], this.fields[2][0]]
        ]);
    }

    * getRows() {
        for (let row of this.fields) {
            yield {data: row, type: 'row'};
        }
    }

    * getColumns() {
        for (let i = 0; i < this.fields.length; i++) {
            yield {
                data: [
                    this.fields[0][i],
                    this.fields[1][i],
                    this.fields[2][i]
                ],
                type: 'column'
            };
        }
    }
    * getDiagonals() {
        yield {
            data: [
                this.fields[0][0],
                this.fields[1][1],
                this.fields[2][2],
            ],
        type: 'diagonal'
        };
        yield {
            data: [
                this.fields[0][2],
                this.fields[1][1],
                this.fields[2][0],
            ],
            type: 'diagonal'
            };
    }

    * getPossibilities() {
        yield* this.getRows();
        yield* this.getColumns();
        yield* this.getDiagonals();
    }

    [Symbol.iterator]() {
        return this.getPossibilities();
    }

    toString(){
        console.table(this.fields);
    }
}


class TicTacToe {
    constructor() {
        this.board = new Board();
        this.currentPlayer = 'X';
        this.gameOver=false;
    }

    placeMark(x, y) {
        if(!this.gameOver) {
            if (this.board.fieldEmpty(x, y)) {
                this.board.place(x, y, this.currentPlayer);

                if (this.checkWin(this.currentPlayer)) {
                    this.gameOver = true;
                    console.log("Game ended, winner is: " + this.currentPlayer);
                } else {
                    if (this.checkTie()){
                        this.gameOver = true;
                        console.log("Game ended. It is a TIE!");
                    }
                }

                this.changePlayer();
            }
        } else {
            console.log("Game over!")
        }
    }

    checkWin(mark) {
        var win = true;
        for(let data of this.board){
            win = true;
            for(let triplet of data.data) {
                if (triplet !== mark) {
                    win = false;
                }
            }
            if (win) {
                console.log("Winning type is: "+data.type);
                return true;
            }
        }
        return win;
    }

    checkTie(){
        for(let data of this.board){
            for(let triplet of data.data) {
                if(!triplet){
                    return false
                }
            }
        }
        return true;
    }

    changePlayer() {
        this.currentPlayer = this.currentPlayer === 'X' ? 'O' : 'X';
    }
}


let game1 = new TicTacToe();
let game2 = new TicTacToe();
let game3 = new TicTacToe();

function checkBound(value, name, min, max) {
    if (typeof value !== 'number' || value < min || value > max) {
        throw new Error(`${name} must be positive number and not greater than ${max}`);
    }
}

//console.log(typeof game1);
//console.log(game1.board instanceof Array);
//console.log(Array.isArray(game1.board));

//Case 1: X will win
console.log("================================");
console.log("   X win case")
console.log("================================");
try {
    game1.placeMark(0, 0);
    game1.placeMark(1, 0);
    game1.placeMark(0, 1);
    game1.placeMark(1, 1);
    game1.placeMark(0, 2);
    game1.placeMark(1, 2);
} catch (e) {
    console.log('Error ', e.message);
}
finally {
    console.log("X win case's Board");
    game1.board.toString();
}


//Case 1: O will win
console.log("================================");
console.log("   O win case")
console.log("================================");
try {
    game2.placeMark(1, 0);
    game2.placeMark(0, 0);
    game2.placeMark(1, 2);
    game2.placeMark(1, 1);
    game2.placeMark(2, 0);
    game2.placeMark(2, 2);
} catch (e) {
    console.log('Error ', e.message);
} finally {
    console.log("O win case's Board");
    game2.board.toString();
}

//Case 1: O will win
console.log("================================");
console.log("   Tie case")
console.log("================================");
try {
    game3.placeMark(0, 0);
    game3.placeMark(0, 2);
    game3.placeMark(0, 1);
    game3.placeMark(1, 0);
    game3.placeMark(1, 2);
    game3.placeMark(1, 1);
    game3.placeMark(2, 0);
    game3.placeMark(2, 2);
    game3.placeMark(2, 1);
} catch (e) {
    console.log('Error ', e.message);
} finally {
    console.log("TIE case's Board");
    game3.board.toString();
}
